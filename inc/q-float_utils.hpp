/*
 * Created by Oskari Teeri on 10/10/21.
 */

#pragma once

/* Works with unsigned integers as a compile-time std::pow alternative. */
template <class T>
constexpr T ipow(T num, unsigned int pow) {
  return (pow >= sizeof(unsigned int) * 8) ? 0
         : pow == 0                        ? 1
                                           : num * ipow(num, pow - 1);
}