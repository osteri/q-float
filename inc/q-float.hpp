#pragma once

#include <cassert>
#include <cmath>

#include <q-float_utils.hpp>

/* TODO: implement compile-time std::pow for negative integers */

template <unsigned short M, unsigned short N, class FloatT = float>
class QFloat {
  using value_t = FloatT;
  using value_int_t = long long int;
  using Q = typename ::QFloat<M, N>;

 private:
  constexpr explicit QFloat(const value_int_t i) : m_value{i} {}
  const value_int_t m_value;

 public:
  constexpr explicit QFloat(const Q& q) : m_value{q.m_value} {}
  constexpr explicit QFloat(const value_t f)
      : m_value{static_cast<value_int_t>(ipow<value_int_t>(2, N) * f)} {
    static_assert(M + N <= 64, "M+N can only be max 64");
    static_assert(M + N <= sizeof(m_value) * 8, "bit size must be <= M+N");
    static_assert(M != 0 || N != 0, "M nor N cannot be 0");
    assert(f > min());
    assert(f < max());
  }
  constexpr QFloat operator+(const Q& rhs) const {
    return Q{static_cast<value_int_t>(m_value + rhs.m_value)};
  }
  constexpr QFloat operator-(const Q& rhs) const {
    return Q{static_cast<value_int_t>(m_value - rhs.m_value)};
  }
  constexpr QFloat operator*(const Q& rhs) const {
    return Q{static_cast<value_int_t>(m_value * rhs.m_value /
                                      ipow<value_int_t>(2, N))};
  }
  constexpr QFloat operator/(const Q& rhs) const {
    return Q{static_cast<value_int_t>(ipow<value_int_t>(2, N) * m_value /
                                      rhs.m_value)};
  }

  constexpr value_t to_float() const {
    return std::pow(2, -N) * static_cast<value_t>(m_value);
  }
  constexpr std::size_t bits_required() const { return M + N; }
  constexpr value_t min() const { return -ipow<value_int_t>(2, M - 1); }
  constexpr value_t max() const {
    return ipow<value_int_t>(2, M - 1) - std::pow(2, -N);
  }
  constexpr value_t get_resolution() const { return std::pow(2, -N); }
};
