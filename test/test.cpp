#include <gtest/gtest.h>
#include "q-float.hpp"

TEST(Q15_1, rounding) { ASSERT_FLOAT_EQ((QFloat<15, 1>(0.5f)).to_float(), 0.5f); }
TEST(Q15_1, min) { ASSERT_FLOAT_EQ((QFloat<15, 1>(0.5f)).min(), -16384.0f); }
TEST(Q15_1, max) { ASSERT_FLOAT_EQ((QFloat<15, 1>(0.5f)).max(), 16383.5f); }
TEST(Q15_1, bits) { ASSERT_EQ((QFloat<15, 1>(0.5f)).bits_required(), 16); }
TEST(Q14_2, rounding) {
  ASSERT_FLOAT_EQ((QFloat<14, 2>(0.25f)).to_float(), 0.25f);
}
TEST(Q14_2, min) { ASSERT_FLOAT_EQ((QFloat<14, 2>(0.5f)).min(), -8192); }
TEST(Q14_2, max) { ASSERT_FLOAT_EQ((QFloat<14, 2>(0.5f)).max(), 8191.75); }
TEST(Q14_2, add) {
  QFloat<14, 2> a(0.5f);
  QFloat<14, 2> b(0.25f);
  ASSERT_FLOAT_EQ((a + b).to_float(), 0.75f);
}
TEST(Q8_8, times) {
  QFloat<8, 8> a(0.5f);
  QFloat<8, 8> b(0.5f);
  ASSERT_FLOAT_EQ((a * b).to_float(), 0.25f);
}
TEST(Q8_8, div) {
  QFloat<8, 8> a(0.5f);
  QFloat<8, 8> b(0.5f);
  ASSERT_FLOAT_EQ((a / b).to_float(), 1.0f);
}
TEST(Q2_14, times_and_div) {
  QFloat<2, 14> a(0.5f);
  QFloat<2, 14> b(0.5f);
  QFloat<2, 14> c(0.25f);
  ASSERT_FLOAT_EQ(((a * b) / c).to_float(), 1.0f);
}
TEST(Q5_11, times_and_div) {
  QFloat<5, 11> a(0.5f);
  QFloat<5, 11> b(0.5f);
  QFloat<5, 11> c(1.0f);
  ASSERT_FLOAT_EQ(((a * b) / c).to_float(), 0.25f);
}
TEST(Q31_1, max) { ASSERT_FLOAT_EQ((QFloat<31, 1>(0.1f)).max(), 1.0737418e+09); }

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
