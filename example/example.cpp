#include <iostream>

#include <q-float.hpp>

int main() {
  QFloat<15, 1> q(0.5f);
  std::cout << "bits required for f: " << q.bits_required() << '\n';
  std::cout << "range for f (min, max): " << q.min() << ", " << q.max()
            << " with resolution of " << q.get_resolution() << '\n';

  /* Answer is calculated at compile-time */
  using Q = QFloat<1, 15>;
  constexpr auto ans = Q{0.2f} + Q{0.3f} + Q{0.2f};
  constexpr float f = ans.to_float();
  std::cout << f << '\n'; // prints: 0.699951

  /* You can also control the underlying type.
   * Note: the constructor now accepts 0.5 instead of 0.5f */
  using Qd = QFloat<1, 15, double>;
  Qd d(0.5);

  return EXIT_SUCCESS;
}
