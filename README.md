# Q number library for embedded systems

Very simple and minimalistic [Q number format](https://en.wikipedia.org/wiki/Q_(number_format)) library using modern `C++11` with templates and `constexpr`. Converts to/from float. This library is type-safe, you cannot accidentally make arithmetic operations between two different Q numbers. Overflow is checked runtime with assert and some sanity checks are done during compile-time.

## Basic usage

```cpp
#include <iostream>
#include "q-float.hpp"

int main() {
  QFloat<15, 1> q(0.5f);
  std::cout << "bits required for f: " << q.bits_required() << '\n';
  std::cout << "range for f (min, max): " << q.min() << ", " << q.max()
            << " with resolution of " << q.get_resolution() << '\n';

  /* Answer is calculated at compile-time */
  using Q = QFloat<1, 15>;
  constexpr auto ans = Q{0.2f} + Q{0.3f} + Q{0.2f};
  constexpr float f = ans.to_float();
  std::cout << f << '\n'; // in my PC prints: 0.699951

  return EXIT_SUCCESS;
}
```

See more usage examples in [test](https://gitlab.com/osteri/q-float/blob/master/test/) and [example](https://gitlab.com/osteri/q-float/blob/master/example/) directories. 

## Adding library as a git submodule in CMake

First clone the library:
```
git submodule add https://gitlab.com/osteri/q-float
```
Then include this library in your main `CMakeLists.txt` like this:
```
add_subdirectory(q-float)
target_link_libraries(main LINK_PUBLIC q-float-lib)
```

## Build all

```
cmake -S . -B build -DBUILD_TESTS=ON -DBUILD_EXAMPLES=ON
cd build && make -j
```
