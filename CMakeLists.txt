cmake_minimum_required(VERSION 3.13.4)
project(q-float CXX)

add_subdirectory(inc)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

if (BUILD_EXAMPLES)
  add_subdirectory(example)
endif()

if (BUILD_TESTS)
  add_subdirectory(ext/googletest)
  add_subdirectory(test)
endif()
